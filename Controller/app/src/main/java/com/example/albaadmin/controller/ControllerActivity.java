package com.example.albaadmin.controller;

import android.content.Context;
import android.icu.text.LocaleDisplayNames;
import android.net.DhcpInfo;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.method.ScrollingMovementMethod;
import android.util.Log;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.TextView;

import java.io.IOException;
import java.math.BigInteger;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.SocketTimeoutException;
import java.net.UnknownHostException;
import java.nio.ByteOrder;

public class ControllerActivity extends AppCompatActivity {

    private String TAG = "SIAF-Controller";
    private DatagramSocket cmdSocket = null;
    private WifiManager wifiManager = null;
    private Thread serverThread = null;
    private final int DISCOVERY_PORT = 12111;
    private String machineAddress = null;

    private boolean DISCOVERING= false;
    private TextView activityLog = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_controller);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });

        wifiManager = (WifiManager) getApplicationContext().getSystemService(Context.WIFI_SERVICE);
        if(!wifiManager.isWifiEnabled()) {
            wifiManager.setWifiEnabled(true);
        }

        String[] ipConfig = ifConfig();
        printBroadcastAddress();
        activityLog = (TextView) findViewById(R.id.activityLog);
        activityLog.setMovementMethod(new ScrollingMovementMethod());
        activityLog.append("Activity log:\n");
        activityLog.append("Controller ipAddress: " + ipConfig[0] + "\n");
        activityLog.append("Broadcast address: " + ipConfig[1] + "\n");
        activityLog.append("MacID address: " + getMacId() + "\n");
        activityLog.append("Gateway IP: " + ipConfig[2] + "\n");

        final Button discoveryButton = (Button) findViewById(R.id.discoveryButton);
        discoveryButton.setOnClickListener(new Button.OnClickListener() {
            public void onClick(View v) {
                if(!DISCOVERING) {
                    DISCOVERING = !DISCOVERING;
                    activityLog.append("\nStarting the discovery...\n");
                    new DiscoveryProcess().execute();
                }else {
                    activityLog.append("Cannot perform a new discovery this time\n");
                }
            }
        });

        Button sendCommandButton = (Button) findViewById(R.id.commandButton);
        sendCommandButton.setOnClickListener(new Button.OnClickListener() {
            public void onClick(View v) {
                if(machineAddress != null)
                    new SendCommand().execute();
                else
                    activityLog.append("No machine found nearby\n");
            }
        });
    }

    public String getMacId() {

        WifiInfo wifiInfo = wifiManager.getConnectionInfo();
        return wifiInfo.getBSSID();
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_controller, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    String[] ifConfig() {
        DhcpInfo dhcp = wifiManager.getDhcpInfo();
        // handle null somehow
        int ipAddress = dhcp.ipAddress;
        int ipGateway    = dhcp.gateway;
        String formattedIpAddress = null;
        String broadcastAddr      = null;
        String formattedIpGateway = null;

        //ipAddress
        if (ByteOrder.nativeOrder().equals(ByteOrder.LITTLE_ENDIAN)) {
            ipAddress = Integer.reverseBytes(dhcp.ipAddress);
        }

        byte[] ipByteArray = BigInteger.valueOf(ipAddress).toByteArray();

        try {
            formattedIpAddress = InetAddress.getByAddress(ipByteArray)
                    .getHostAddress();
        } catch (UnknownHostException ex) {
            Log.d(TAG, "Unable to get host address.");
            formattedIpAddress = "NaN";
        }

        //ipMask
        int broadcast = (dhcp.ipAddress & dhcp.netmask) | ~dhcp.netmask;
        byte[] quads = new byte[4];
        for (int k = 0; k < 4; k++)
            quads[k] = (byte) ((broadcast >> k * 8) & 0xFF);

        try {
            broadcastAddr =  InetAddress.getByAddress(quads).getHostAddress();
        }catch(Throwable thr) {
            Log.d(TAG, "Tried to format IP address but did not succeed");
        }

        //ipGateway
        if (ByteOrder.nativeOrder().equals(ByteOrder.LITTLE_ENDIAN)) {
            ipGateway = Integer.reverseBytes(dhcp.gateway);
        }

        byte[] ipGatewayByteArray = BigInteger.valueOf(ipGateway).toByteArray();

        try {
            formattedIpGateway = InetAddress.getByAddress(ipGatewayByteArray)
                    .getHostAddress();
        } catch (UnknownHostException ex) {
            Log.d(TAG, "Unable to get gateway address.");
            formattedIpGateway = "NaN";
        }

        return new String[]{formattedIpAddress, broadcastAddr, formattedIpGateway};

    }
    void printBroadcastAddress() {
        DhcpInfo dhcp = wifiManager.getDhcpInfo();
        // handle null somehow

        int broadcast = (dhcp.ipAddress & dhcp.netmask) | ~dhcp.netmask;
        byte[] quads = new byte[4];
        for (int k = 0; k < 4; k++)
            quads[k] = (byte) ((broadcast >> k * 8) & 0xFF);

        try {
            Log.d(TAG, "TentativeNet " + InetAddress.getByAddress(quads).getHostAddress());
        }catch(Throwable thr) {
            Log.d(TAG, "Tried to format IP address but did not succeed");
        }
    }

    private class DiscoveryProcess extends AsyncTask<String, Void, String> {


        @Override
        protected String doInBackground(String... params) {

            byte[] buffer = new byte[48];
            DatagramPacket recv = new DatagramPacket(buffer, buffer.length);
            DatagramSocket socket = null;
            //kickstart discovery process
            try {//Open a random port to send the package
                socket = new DatagramSocket(DISCOVERY_PORT);
                socket.setBroadcast(true);
                byte[] sendData = "Discover".getBytes();
                DatagramPacket sendPacket = new DatagramPacket(sendData, sendData.length, getBroadcastAddress(), DISCOVERY_PORT);
                Log.d(TAG,"Broadcasting a discovery packet");
                socket.send(sendPacket);
                Log.d(TAG,"Broadcasted a discovery packet");
            }catch(IOException ioex) {
                return "";
            }
            //listen for replies
            StringBuilder machineAddresses = new StringBuilder();
            String ipAddress = "";
            boolean received = false;
            String strMessage = null;
            try {
                socket.setSoTimeout(500000);//5 minutes.
                while(true) {
                    try {
                        socket.receive(recv);
                        //received = true;
                        if (recv.getData().length > 0) {
                            strMessage = new String(recv.getData(), 0, recv.getLength());
                            Log.d(TAG, "Received this amount of data: " + recv.getLength());
                            Log.d(TAG, "Received a machineId of byteSize: " + recv.getLength() + " and resulting ip:" + strMessage+ "\n");
                            //B.ipAddress = InetAddress.getByAddress(recv.getData())
                            //        .getHostAddress();
                            //machineAddresses.append(ipAddress + "\n");
                            machineAddresses.append(strMessage + "\n");
                        }
;                   }catch(UnknownHostException unex) {
                        machineAddresses.append("\nIll formatted IPVX address");
                    }catch (SocketTimeoutException e) {
                        /* no response received after 10 second. quit!*/
                        break;
                    } catch (Throwable thr) {
                        // TODO Auto-generated catch block
                        thr.printStackTrace();

                    }
                }

            } catch (IOException e) {
                Log.e(TAG, "IOException: " + e.getMessage());
            }

            socket.close();

            DISCOVERING = false;
            return machineAddresses.toString();
        }

        InetAddress getBroadcastAddress() throws IOException {
            DhcpInfo dhcp = wifiManager.getDhcpInfo();
            // handle null somehow

            int broadcast = (dhcp.ipAddress & dhcp.netmask) | ~dhcp.netmask;
            byte[] quads = new byte[4];
            for (int k = 0; k < 4; k++)
                quads[k] = (byte) ((broadcast >> k * 8) & 0xFF);
            return InetAddress.getByAddress(quads);
        }


        @Override
        protected void onPostExecute(String result) {
            activityLog.append("Discovered the following machines#: " + result + "\n"); // txt.setText(result);
            // might want to change "executed" for the returned string passed
            // into onPostExecute() but that is upto you
        }

        @Override
        protected void onPreExecute() {}

        @Override
        protected void onProgressUpdate(Void... values) {}
    }

    private class SendCommand extends AsyncTask<String, Void, String> {


        @Override
        protected String doInBackground(String... params) {

            try {
                //Open a random port to send the package
                DatagramSocket socket = new DatagramSocket();
                byte[] sendData = "Command".getBytes();
                DatagramPacket sendPacket = new DatagramPacket(sendData, sendData.length, InetAddress.getByAddress(machineAddress.getBytes()), DISCOVERY_PORT+1);
                socket.send(sendPacket);

            } catch (IOException e) {
                Log.e(TAG, "IOException: " + e.getMessage());
            }
            return "";
        }

        @Override
        protected void onPostExecute(String result) {
            activityLog.setText("Sent a new command to machine\n"); // txt.setText(result);
        }

        @Override
        protected void onPreExecute() {}

        @Override
        protected void onProgressUpdate(Void... values) {}
    }
}

