package com.example.appliance;

import android.content.Context;
import android.net.DhcpInfo;
import android.net.wifi.WifiManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.TextView;

import java.io.IOException;
import java.math.BigInteger;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.SocketException;
import java.net.SocketTimeoutException;
import java.net.UnknownHostException;
import java.nio.ByteOrder;

import androidx.appcompat.app.AppCompatActivity;

public class MainActivity extends AppCompatActivity {

    private String TAG = "SIAF-Appliance";
    private DatagramSocket cmdSocket = null;
    private WifiManager wifiManager = null;
    private Thread serverThread = null;
    private final int DISCOVERY_PORT = 12111;
    private String machineAddressStr = null;
    private int machineAddressInt;

    private boolean listening = false;
    private TextView activityLog = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_main);

        wifiManager = (WifiManager) getApplicationContext().getSystemService(Context.WIFI_SERVICE);
        if(!wifiManager.isWifiEnabled()) {
            wifiManager.setWifiEnabled(true);
        }

        activityLog = (TextView) findViewById(R.id.activityLog);
        activityLog.append("Activity log:\n");

        //XXX: discovery own ip address.
        try {
            String[] ipConfig = ifConfig();
            machineAddressStr = ipConfig[0];
            machineAddressInt = getOwnIPAddressInt();
            activityLog.append("Own address: " + machineAddressStr + "\n");
            activityLog.append("Own mask: " + ipConfig[1] + "\n");
            activityLog.append("Own gateway: " + ipConfig[2] + "\n");
        }catch(IOException ioex) {

        }

        Button listenDiscoveryButton = (Button) findViewById(R.id.listenButton);
        listenDiscoveryButton.setOnClickListener(new Button.OnClickListener() {
            public void onClick(View v) {
                if(!listening) {
                    listening = !listening;
                    activityLog.append("Listening to broadcast channel\n");
                    new DiscoveryProcess().execute();
                }else {
                    activityLog.append("Already listening to discovery\n");
                }
            }
        });

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    String[] ifConfig() {
        DhcpInfo dhcp = wifiManager.getDhcpInfo();
        // handle null somehow
        int ipAddress = 0;
        int ipMask    = 0;
        int ipGateway    = 0;
        String formattedIpAddress = null;
        String formattedIpMask = null;
        String formattedIpGateway = null;

        //ipAddress
        if (ByteOrder.nativeOrder().equals(ByteOrder.LITTLE_ENDIAN)) {
            ipAddress = Integer.reverseBytes(dhcp.ipAddress);
        }

        byte[] ipByteArray = BigInteger.valueOf(ipAddress).toByteArray();

        try {
            formattedIpAddress = InetAddress.getByAddress(ipByteArray)
                    .getHostAddress();
        } catch (UnknownHostException ex) {
            Log.e("WIFI_IP", "Unable to get host address.");
            formattedIpAddress = "NaN";
        }

        //ipMask
        if (ByteOrder.nativeOrder().equals(ByteOrder.LITTLE_ENDIAN)) {
            ipMask = Integer.reverseBytes(dhcp.netmask);
        }

        byte[] ipMaskByteArray = BigInteger.valueOf(ipMask).toByteArray();

        try {
            formattedIpMask = InetAddress.getByAddress(ipByteArray)
                    .getHostAddress();
        } catch (UnknownHostException ex) {
            Log.e("WIFI_IP", "Unable to get host address.");
            formattedIpMask = "NaN";
        }

        //ipGateway
        if (ByteOrder.nativeOrder().equals(ByteOrder.LITTLE_ENDIAN)) {
            ipMask = Integer.reverseBytes(dhcp.netmask);
        }

        byte[] ipGatewayByteArray = BigInteger.valueOf(ipGateway).toByteArray();

        try {
            formattedIpGateway = InetAddress.getByAddress(ipGatewayByteArray)
                    .getHostAddress();
        } catch (UnknownHostException ex) {
            Log.e("WIFI_IP", "Unable to get host address.");
            formattedIpGateway = "NaN";
        }

        return new String[]{formattedIpAddress, formattedIpMask, formattedIpGateway};

    }

    int getOwnIPAddressInt() throws IOException {
        DhcpInfo dhcp = wifiManager.getDhcpInfo();
        // handle null somehow
    return dhcp.ipAddress;
    }


    InetAddress getBroadcastAddress() throws IOException {
        DhcpInfo dhcp = wifiManager.getDhcpInfo();
        // handle null somehow

        int broadcast = (dhcp.ipAddress & dhcp.netmask) | ~dhcp.netmask;
        byte[] quads = new byte[4];
        for (int k = 0; k < 4; k++)
            quads[k] = (byte) ((broadcast >> k * 8) & 0xFF);
        return InetAddress.getByAddress(quads);
    }

    private class DiscoveryProcess extends AsyncTask<String, Void, String> {


        //listens for discovery a packet in a pre-determined port. When received
        // asnwers back with its own ip address.
        @Override
        protected String doInBackground(String... params) {

            //B.byte[] ipByteArray = BigInteger.valueOf(machineAddressInt).toByteArray();
            byte[] buffer = new byte[1024];
            DatagramPacket recv = new DatagramPacket(buffer, buffer.length);
            DatagramSocket socket = null;
            try {
                socket = new DatagramSocket(DISCOVERY_PORT);
                socket.setBroadcast(true);
            } catch (SocketException e) {
                e.printStackTrace();
            }
            //listen for a discovery packet.
            try {
                socket.receive(recv);
                Log.d(TAG, "Received a discovery packet: " + new String(recv.getData(), 0, recv.getLength()));
            } catch (IOException e) {
                Log.e(TAG, "IOException: " + e.getMessage());
            }

            //return back response
            try {//Open a random port to send the package
                //B.DatagramPacket sendPacket = new DatagramPacket(ipByteArray, ipByteArray.length, getBroadcastAddress(), DISCOVERY_PORT);
                byte[] ipAddressMsg = machineAddressStr.getBytes();
                DatagramPacket sendPacket = new DatagramPacket(ipAddressMsg, ipAddressMsg.length, getBroadcastAddress(), DISCOVERY_PORT);
                Log.d(TAG, "Sending back my own ip address as a response");
                socket.send(sendPacket);
                Log.d(TAG, "Broadcasted my own ip address");
                socket.close();
            } catch (IOException ioex) {
                ioex.printStackTrace();
            }
            listening = false;
            return "OK";
        }


        @Override
        protected void onPostExecute(String result) {
            activityLog.append("Successfully responded to the discovery process\n"); // txt.setText(result);
            // might want to change "executed" for the returned string passed
            // into onPostExecute() but that is upto you
        }

        @Override
        protected void onPreExecute() {
        }

        @Override
        protected void onProgressUpdate(Void... values) {
        }
    }
}

